package io.dflabs.gastalon.views.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.utils.SnackbarUtils;

/**
 * Created by Daniel García Alvarado on 9/3/15.
 * Gastalon - danielgarcia
 */
public class BaseActivity extends AppCompatActivity{

    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;
    protected EventBus mBus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBus = EventBus.getDefault();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        setupToolbar();
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    protected void setupToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Subscribe
    public void onEvent(Object object){}


    public void showError(int message) {
        SnackbarUtils.error(this, message);
    }
}
