package io.dflabs.gastalon.views.activities;

import android.graphics.Bitmap;
import android.os.Bundle;

import io.dflabs.gastalon.presenters.AddTransactionPresenter;
import io.dflabs.lib.validators.FormValidator;

/**
 * Created by Daniel García Alvarado on 9/14/15.
 * Gastalon - danielgarcia
 */
public abstract class TransactionActivity extends BaseActivity{

    protected FormValidator mFormValidator;
    protected AddTransactionPresenter transactionPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFormValidator = new FormValidator(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        transactionPresenter.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        transactionPresenter.onPause();
    }

    public boolean formValid() {
        return mFormValidator.isValid();
    }

    public abstract String getTitleTransaction();

    public abstract String getAmountTransaction();

    public abstract String getNoteTransaction();

    public void close() {
        setResult(RESULT_OK);
        finish();
    }

    public abstract void addImageToLayoutContainer(Bitmap scaledBitmap);
}
