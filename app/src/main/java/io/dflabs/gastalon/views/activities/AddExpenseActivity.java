package io.dflabs.gastalon.views.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.enums.KindOfTransaction;
import io.dflabs.gastalon.presenters.AddTransactionPresenter;
import io.dflabs.gastalon.utils.CameraUtils;
import io.dflabs.gastalon.utils.GraphicUtils;
import io.dflabs.lib.validators.EditTextValidator;
import io.dflabs.lib.validators.FormValidator;
import io.dflabs.lib.validators.Regex;

/**
 * Created by Daniel García Alvarado on 9/3/15.
 * Gastalon - danielgarcia
 */
public class AddExpenseActivity extends TransactionActivity {

    @Bind(R.id.act_add_expense_title)
    EditText mTitleEditText;
    @Bind(R.id.act_add_expense_amount)
    EditText mAmountEditText;
    @Bind(R.id.act_add_expense_more_container)
    LinearLayout mOptionsLinearLayout;
    @Bind(R.id.act_add_expense_note)
    EditText mNoteEditText;
    @Bind(R.id.act_add_expense_photos_container)
    LinearLayout mPhotosLinearLayout;
    @Bind(R.id.act_add_expense_add_photo)
    FrameLayout mAddPhotoLayout;
    @Bind(R.id.act_add_expense_more)
    Button mLoadMoreButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_expense);
        ButterKnife.bind(this);
        transactionPresenter = new AddTransactionPresenter(this, KindOfTransaction.EXPENSE);
        registerForContextMenu(mAddPhotoLayout);
        registerFormControls();
    }

    private void registerFormControls() {
        mFormValidator = new FormValidator(this);
        mFormValidator.addValidators(
                new EditTextValidator(mTitleEditText, Regex.NOT_EMPTY, R.string.error_empty),
                new EditTextValidator(mTitleEditText, Regex.ALPHA_NUMERIC, R.string.error_format),
                new EditTextValidator(mAmountEditText, Regex.NOT_EMPTY, R.string.error_empty),
                new EditTextValidator(mAmountEditText, Regex.DECIMAL, R.string.error_format)
        );
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chooser_photo, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_take_photo:
                CameraUtils.takePhoto(this);
                return true;
            case R.id.action_import_photo:
                CameraUtils.importPhoto(this);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    @OnClick(R.id.act_add_expense_save)
    void onSaveClick() {
        transactionPresenter.saveTransaction();
    }

    @OnClick(R.id.act_add_expense_more)
    void onMoreClick() {
        boolean hidden = transactionPresenter.toggleHiddenExtraFields();
        mOptionsLinearLayout.setVisibility(hidden ? View.GONE : View.VISIBLE);
        mLoadMoreButton.setText(hidden ? R.string.act_add_load_more : R.string.act_add_load_less);
    }

    @OnClick(R.id.act_add_expense_add_photo)
    void onAddPhotoContainer() {
        mAddPhotoLayout.showContextMenu();
    }

    public void addImageToLayoutContainer(Bitmap thumbnail) {
        ImageView imageView = new ImageView(this);
        int size = GraphicUtils.convertDpToPx(this, 80);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(size, size);
        params.setMargins(0, 0, 8, 0);
        imageView.setLayoutParams(params);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageBitmap(thumbnail);
        mPhotosLinearLayout.addView(imageView);
    }

    public void close() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CameraUtils.REQUEST_TAKE_PHOTO) {
            if (resultCode == RESULT_OK) {
                transactionPresenter.addImage(CameraUtils.fullImage());
            }
        } else if (requestCode == CameraUtils.REQUEST_IMPORT_PHOTO) {
            if (resultCode == RESULT_OK) {
                transactionPresenter.addImage(CameraUtils.fullImageFromIntent(this, data));
            }
        }
    }

    @Override
    public String getTitleTransaction() {
        return mTitleEditText.getText().toString().trim();
    }

    @Override
    public String getAmountTransaction() {
        return mAmountEditText.getText().toString().trim();
    }

    @Override
    public String getNoteTransaction() {
        return mNoteEditText.getText().toString().trim();
    }
}
