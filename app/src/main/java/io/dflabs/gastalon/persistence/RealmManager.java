package io.dflabs.gastalon.persistence;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by Daniel García Alvarado on 9/3/15.
 * Gastalon - danielgarcia
 */
public class RealmManager {

    public static <T extends RealmObject> RealmList<T> list(Context context, Class<T> aClass) {
        RealmResults<T> results = Realm.getInstance(context)
                .where(aClass).findAll();
        RealmList<T> realmList = new RealmList<>();
        for (T result : results) {
            realmList.add(result);
        }
        return realmList;
    }
}
