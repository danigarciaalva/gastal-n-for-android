package io.dflabs.gastalon.serializers;

import android.location.Location;
import android.location.LocationProvider;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/**
 * Created by Daniel García Alvarado on 8/30/15.
 * Gastalon - danielgarcia
 */
public class LocationSerializer implements JsonSerializer<Location>, JsonDeserializer<Location> {
    @Override
    public JsonElement serialize(Location src, Type typeOfSrc, JsonSerializationContext context) {
        return new JsonPrimitive(src.getLatitude() + "," + src.getLongitude());
    }


    @Override
    public Location deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
            throws JsonParseException {
        String jsonString = json.getAsString();
        if (jsonString != null && !jsonString.isEmpty()) {
            if (jsonString.split(",").length == 2) {
                Location location = new Location("");
                location.setLatitude(Double.parseDouble(jsonString.split(",")[0]));
                location.setLongitude(Double.parseDouble(jsonString.split(",")[1]));
                return location;
            }
        }
        return null;
    }
}
