package io.dflabs.gastalon.models;

import android.content.Context;

import java.util.UUID;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 9/3/15.
 * Gastalon - danielgarcia
 */
public class Income extends RealmObject{

    @PrimaryKey
    private String id;
    private Transaction transaction;
    private RealmList<Image> images;
    private double latitude;
    private double longitude;
    private String note;

    public Income() {
        id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public void setImages(RealmList<Image> images) {
        this.images = images;
    }

    public RealmList<Image> getImages() {
        return images;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getNote() {
        return note;
    }

    public static class Builder {

        private String title;
        private float amount;
        private Category category;
        private RealmList<Image> images;
        private double latitude;
        private double longitude;
        private String note;

        public Builder() {

        }

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder amount(float amount) {
            this.amount = amount;
            return this;
        }

        public Builder category(Category category) {
            this.category = category;
            return this;
        }

        public Income create() {
            Income income = new Income();
            Transaction transaction = new Transaction();
            if (this.title != null) transaction.setTitle(this.title);
            transaction.setAmount(this.amount);
            if (category != null) transaction.setCategory(this.category);
            income.setTransaction(transaction);
            income.setImages(images);
            income.setLatitude(latitude);
            income.setLongitude(longitude);
            income.setNote(note);
            return income;
        }

        public Builder images(RealmList<Image> images) {
            this.images = images;
            return this;
        }

        public Builder latitude(double latitude) {
            this.latitude = latitude;
            return this;
        }

        public Builder longitude(double longitude) {
            this.longitude = longitude;
            return this;
        }

        public Builder note(String note) {
            this.note = note;
            return this;
        }
    }
}
