package io.dflabs.gastalon.models;

import java.util.Date;
import java.util.UUID;

import io.realm.RealmList;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 9/14/15.
 * Gastalon - danielgarcia
 */
public class Account {

    @PrimaryKey
    private String id;
    private String name;
    private String coverImage;
    private String color;
    private double balance;
    private Date createdAt;
    private Date modifiedAt;
    private RealmList<Income> incomes;
    private RealmList<Expense> expenses;

    public Account(){
        id = UUID.randomUUID().toString();
        incomes = new RealmList<>();
        expenses = new RealmList<>();
        createdAt = new Date();
        modifiedAt = new Date();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }


    public RealmList<Income> getIncomes() {
        return incomes;
    }

    public void setIncomes(RealmList<Income> incomes) {
        this.incomes = incomes;
    }

    public RealmList<Expense> getExpenses() {
        return expenses;
    }

    public void setExpenses(RealmList<Expense> expenses) {
        this.expenses = expenses;
    }


}

