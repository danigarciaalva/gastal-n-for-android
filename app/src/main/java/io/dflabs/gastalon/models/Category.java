package io.dflabs.gastalon.models;

import java.util.UUID;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Daniel García Alvarado on 9/3/15.
 * Gastalon - danielgarcia
 */
public class Category extends RealmObject{

    @PrimaryKey
    private String id;
    private String name;

    public Category(){
        this.id = UUID.randomUUID().toString();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
