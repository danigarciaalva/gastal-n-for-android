package io.dflabs.gastalon.ws.requests;

import android.location.Location;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Daniel García Alvarado on 8/29/15.
 * Gastalon - danielgarcia
 */
public class WSLocationRequest {

    private Location location;
    @SerializedName("device_id")
    private String deviceId;

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
