package io.dflabs.gastalon.ws;

import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.dflabs.gastalon.serializers.LocationSerializer;
import io.dflabs.gastalon.ws.requests.WSLocationRequest;
import io.dflabs.gastalon.ws.responses.BaseResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.Body;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Daniel García Alvarado on 8/28/15.
 * Gastalon - danielgarcia
 */
public class WebServices {
    private static WebServicesDefinitionV1 instance;

    public static WebServicesDefinitionV1 v1() {

        if(instance == null){
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(Location.class, new LocationSerializer()).create();
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint("http://gastalon.dflabs.io/api/v1")
                    .setConverter(new GsonConverter(gson))
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();
            instance = adapter.create(WebServicesDefinitionV1.class);
        }
        return instance;
    }

    public interface WebServicesDefinitionV1{

        @POST("/locations/")
        void location(@Body WSLocationRequest request, Callback<BaseResponse> callback);

    }
}
