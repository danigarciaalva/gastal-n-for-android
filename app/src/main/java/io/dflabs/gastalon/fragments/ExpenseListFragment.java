package io.dflabs.gastalon.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.views.activities.AddExpenseActivity;
import io.dflabs.gastalon.adapters.ExpenseAdapter;
import io.dflabs.gastalon.models.Expense;
import io.dflabs.gastalon.persistence.RealmManager;
import io.dflabs.gastalon.utils.IntentUtils;

/**
 * Created by Daniel García Alvarado on 9/4/15.
 * Gastalon - danielgarcia
 */
public class ExpenseListFragment extends RecyclerFragment{

    private static final int REQUEST_CODE_ADD_EXPENSE = 12345;

    public static ExpenseListFragment newInstance() {
        return new ExpenseListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_expenses_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setLayoutManager(new LinearLayoutManager(mContext));
        setAdapter(new ExpenseAdapter());
        setOnRefreshListener(this);
        setEmptyText(R.string.fr_expenses_empty);
        startRefreshing();
    }

    @Override
    public void onRefresh() {
        addItems(RealmManager.list(mContext, Expense.class));
        notifyDataSetChanged();
        stopRefreshing();
    }

    @OnClick(R.id.fr_expenses_add) void onAddClick(){
        IntentUtils.startActivityForResult(this, AddExpenseActivity.class, REQUEST_CODE_ADD_EXPENSE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_ADD_EXPENSE){
            if(resultCode == Activity.RESULT_OK){
                startRefreshing();
            }
        }
    }
}
