package io.dflabs.gastalon.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import de.greenrobot.event.Subscribe;

/**
 * Created by Daniel García Alvarado on 8/27/15.
 * Gastalon - danielgarcia
 */
public class BaseFragment extends Fragment{

    protected AppCompatActivity mActivity;
    protected Context mContext;
    private EventBus mBus;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mActivity = (AppCompatActivity) getActivity();
        this.mContext = getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBus = EventBus.getDefault();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mBus.unregister(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Subscribe
    public void onEvent(Object object){}
}
