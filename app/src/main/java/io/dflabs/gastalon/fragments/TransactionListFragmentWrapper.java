package io.dflabs.gastalon.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.dflabs.gastalon.R;
import io.dflabs.gastalon.enums.KindOfTransaction;

/**
 * Created by Daniel García Alvarado on 9/14/15.
 * Gastalon - danielgarcia
 */
public class TransactionListFragmentWrapper extends BaseFragment{

    public static TransactionListFragmentWrapper newInstance(KindOfTransaction kindOfTransaction) {
        Bundle args = new Bundle();
        args.putSerializable("transaction", kindOfTransaction);
        TransactionListFragmentWrapper fragment = new TransactionListFragmentWrapper();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_transaction_wrapper, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        KindOfTransaction kindOfTransaction = (KindOfTransaction) getArguments().get("transaction");
        if(kindOfTransaction == KindOfTransaction.EXPENSE) {
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fr_wrapper_container, ExpenseListFragment.newInstance())
                    .commit();
        }else{
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.fr_wrapper_container, IncomesListFragment.newInstance())
                    .commit();
        }
    }
}
