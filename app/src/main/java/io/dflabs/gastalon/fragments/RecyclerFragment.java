package io.dflabs.gastalon.fragments;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.Bind;
import io.dflabs.gastalon.R;
import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Daniel García Alvarado on 8/27/15.
 * Gastalon - danielgarcia
 */
public abstract class RecyclerFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener{

    @Bind(R.id.recycler_view)
    protected RecyclerView mRecyclerView;
    @Bind(R.id.empty_text)
    TextView mEmptyTextView;
    @Bind(R.id.empty)
    FrameLayout mEmptyLayout;
    @Bind(R.id.swipe_to_refresh)
    protected SwipeRefreshLayout mSwipeRefreshLayout;
    protected SwipeRefreshLayout.OnRefreshListener mOnRefreshListener;
    protected SuperRecyclerAdapter mAdapter;

    protected void setLayoutManager(RecyclerView.LayoutManager layoutManager){
        this.mRecyclerView.setLayoutManager(layoutManager);
    }

    protected void setEmptyView(View v){
        mEmptyLayout.removeAllViews();
        mEmptyLayout.addView(v);
    }

    protected void setEmptyText(int text){
        mEmptyTextView.setText(text);
    }

    protected void setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener onRefreshListener){
        this.mOnRefreshListener = onRefreshListener;
        this.mSwipeRefreshLayout.setOnRefreshListener(mOnRefreshListener);
    }

    protected <T extends RecyclerView.Adapter & SuperRecyclerAdapter>void setAdapter(T adapter){
        this.mAdapter = adapter;
        this.mRecyclerView.setAdapter((RecyclerView.Adapter) mAdapter);
    }

    protected <T extends RealmObject>void addItems(RealmList<T> collection){
        mEmptyLayout.setVisibility(collection.size() == 0 ? View.VISIBLE : View.GONE);
        mRecyclerView.setVisibility(collection.size() > 0 ? View.VISIBLE : View.GONE);
        mAdapter.addAll(collection);
    }

    protected void notifyDataSetChanged(){
        ((RecyclerView.Adapter)mAdapter).notifyDataSetChanged();
    }

    protected void startRefreshing(){
        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                mOnRefreshListener.onRefresh();
            }
        });
    }

    protected void stopRefreshing(){
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mOnRefreshListener == null){
            throw new NullPointerException("You must call setOnRefreshListener() within " +
                    "onCreate | onCreateView | onActivityCreated");
        }
        if(mAdapter == null){
            throw new NullPointerException("You must call setAdapter() within " +
                    "onCreate | onCreateView | onActivityCreated");
        }
    }

    public interface SuperRecyclerAdapter<T extends RealmObject>{
        void addAll(RealmList<T> items);
    }
}
