package io.dflabs.gastalon.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.adapters.IncomeAdapter;
import io.dflabs.gastalon.models.Income;
import io.dflabs.gastalon.persistence.RealmManager;
import io.dflabs.gastalon.utils.IntentUtils;
import io.dflabs.gastalon.views.activities.AddIncomeActivity;

/**
 * Created by Daniel García Alvarado on 9/5/15.
 * Gastalon - danielgarcia
 */
public class IncomesListFragment extends RecyclerFragment{

    private static final int REQUEST_CODE_ADD_INCOME = 12346;

    public static IncomesListFragment newInstance() {
        return new IncomesListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_incomes_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        setLayoutManager(new LinearLayoutManager(mContext));
        setAdapter(new IncomeAdapter());
        setOnRefreshListener(this);
        setEmptyText(R.string.fr_incomes_empty);
        startRefreshing();
    }

    @Override
    public void onRefresh() {
        addItems(RealmManager.list(mContext, Income.class));
        notifyDataSetChanged();
        stopRefreshing();
    }

    @OnClick(R.id.fr_incomes_add) void onAddClick(){
        IntentUtils.startActivityForResult(this, AddIncomeActivity.class, REQUEST_CODE_ADD_INCOME);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_CODE_ADD_INCOME){
            if(resultCode == Activity.RESULT_OK){
                startRefreshing();
            }
        }
    }
}
