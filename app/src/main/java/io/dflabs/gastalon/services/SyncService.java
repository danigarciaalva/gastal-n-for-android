package io.dflabs.gastalon.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.telephony.TelephonyManager;
import android.util.Log;

import io.dflabs.gastalon.ws.WebServices;
import io.dflabs.gastalon.ws.requests.WSLocationRequest;
import io.dflabs.gastalon.ws.responses.BaseResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Daniel García Alvarado on 8/28/15.
 * Gastalon - danielgarcia
 */
public class SyncService extends IntentService {
    private static final String TAG = SyncService.class.getSimpleName();
    public static final String ACTION_GEOLOCATION = "sync_geolocation";

    public SyncService() {
        super(TAG);
    }

    public static void requestSync(Context context, String action) {
        Intent intent = new Intent(context, SyncService.class);
        intent.setAction(action);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent != null ? intent.getAction() : null;
        if (ACTION_GEOLOCATION.equals(action)) {
            sendLocationToServer();
        }
    }

    private void sendLocationToServer() {
        Location location = LocationService.getLastLocation();
        if (location != null) {
            WSLocationRequest request = new WSLocationRequest();
            request.setLocation(location);
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
            request.setDeviceId(telephonyManager.getDeviceId());
            WebServices.v1().location(request, new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    Log.d(TAG, "" + response.getStatus());
                }

                @Override
                public void failure(RetrofitError error) {
                    Log.e(TAG, error.getResponse().getReason());
                }
            });
        }
    }
}
