package io.dflabs.gastalon.enums;

/**
 * Created by Daniel García Alvarado on 9/14/15.
 * Gastalon - danielgarcia
 */
public enum KindOfTransaction {
    EXPENSE,
    INCOME
}
