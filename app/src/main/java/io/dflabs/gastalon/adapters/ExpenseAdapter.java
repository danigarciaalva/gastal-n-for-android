package io.dflabs.gastalon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.fragments.RecyclerFragment;
import io.dflabs.gastalon.models.Expense;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/27/15.
 * Gastalon - danielgarcia
 */
public class ExpenseAdapter extends RecyclerView.Adapter<ExpenseAdapter.ViewHolder>
        implements RecyclerFragment.SuperRecyclerAdapter<Expense> {

    private RealmList<Expense> mItems;

    public ExpenseAdapter() {
        mItems = new RealmList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_expense, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Expense expense = mItems.get(position);
        holder.nameTextView.setText(expense.getTransaction().getTitle());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void addAll(RealmList<Expense> items) {
        this.mItems = items;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_expense_name)
        TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
