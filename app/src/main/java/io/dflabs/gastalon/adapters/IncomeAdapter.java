package io.dflabs.gastalon.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.fragments.RecyclerFragment;
import io.dflabs.gastalon.models.Income;
import io.realm.RealmList;

/**
 * Created by Daniel García Alvarado on 8/27/15.
 * Gastalon - danielgarcia
 */
public class IncomeAdapter extends RecyclerView.Adapter<IncomeAdapter.ViewHolder>
        implements RecyclerFragment.SuperRecyclerAdapter<Income> {

    private List<Income> mItems;

    public IncomeAdapter() {
        mItems = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_income, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Income income = mItems.get(position);
        holder.nameTextView.setText(income.getTransaction().getTitle());
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void addAll(RealmList<Income> items) {
        this.mItems = items;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.item_income_name)
        TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
