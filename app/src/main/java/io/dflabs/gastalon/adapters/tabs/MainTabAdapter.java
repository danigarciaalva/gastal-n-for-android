package io.dflabs.gastalon.adapters.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import io.dflabs.gastalon.enums.KindOfTransaction;
import io.dflabs.gastalon.fragments.TransactionListFragmentWrapper;

/**
 * Created by Daniel García Alvarado on 9/2/15.
 * Gastalon - danielgarcia
 */
public class MainTabAdapter extends FragmentStatePagerAdapter {

    public MainTabAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return TransactionListFragmentWrapper.newInstance(KindOfTransaction.EXPENSE);
            case 1:
                return TransactionListFragmentWrapper.newInstance(KindOfTransaction.INCOME);
        }
        return new Fragment();
    }

    @Override
    public int getCount() {
        return 2;
    }
}
