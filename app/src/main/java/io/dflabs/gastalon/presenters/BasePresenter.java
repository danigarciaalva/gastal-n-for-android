package io.dflabs.gastalon.presenters;

import android.support.v7.app.AppCompatActivity;

/**
 * Created by Daniel García Alvarado on 9/13/15.
 * Gastalon - danielgarcia
 */
public abstract class BasePresenter<T extends AppCompatActivity> {

    T activity;

    public BasePresenter(T activity){
        this.activity = activity;
    }

    public abstract void onResume();
    public abstract void onPause();
}
