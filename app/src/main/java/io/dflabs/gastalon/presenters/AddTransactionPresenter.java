package io.dflabs.gastalon.presenters;

import android.graphics.Bitmap;
import android.location.Location;

import java.util.Date;

import de.greenrobot.event.Subscribe;
import io.dflabs.gastalon.R;
import io.dflabs.gastalon.enums.KindOfTransaction;
import io.dflabs.gastalon.models.Expense;
import io.dflabs.gastalon.models.Image;
import io.dflabs.gastalon.models.Income;
import io.dflabs.gastalon.services.LocationService;
import io.dflabs.gastalon.utils.CameraUtils;
import io.dflabs.gastalon.views.activities.TransactionActivity;
import io.realm.Realm;
import io.realm.RealmList;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Daniel García Alvarado on 9/13/15.
 * Gastalon - danielgarcia
 */
public class AddTransactionPresenter extends BasePresenter<TransactionActivity> {

    private final KindOfTransaction kindOfTransaction;
    private Location mLocation;
    private RealmList<Image> mImages;
    private boolean hiddenFields;

    public AddTransactionPresenter(TransactionActivity activity, KindOfTransaction kindOfTransaction) {
        super(activity);
        this.kindOfTransaction = kindOfTransaction;
        hiddenFields = true;
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    public void saveTransaction() {
        if(activity.formValid()) {
            Realm.getInstance(activity).executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if(kindOfTransaction == KindOfTransaction.EXPENSE) {
                        Expense.Builder builder = new Expense.Builder()
                                .title(activity.getTitleTransaction())
                                .amount(Float.parseFloat(activity.getAmountTransaction()));
                        if (mLocation != null) {
                            builder.latitude(mLocation.getLatitude())
                                    .longitude(mLocation.getLongitude());
                        }
                        if (!hiddenFields) {
                            builder.images(mImages);
                            builder.note(activity.getNoteTransaction());
                        }
                        realm.copyToRealmOrUpdate(builder.create());
                    }else{
                        Income.Builder builder = new Income.Builder()
                                .title(activity.getTitleTransaction())
                                .amount(Float.parseFloat(activity.getAmountTransaction()));
                        if (mLocation != null) {
                            builder.latitude(mLocation.getLatitude())
                                    .longitude(mLocation.getLongitude());
                        }
                        if (!hiddenFields) {
                            builder.images(mImages);
                            builder.note(activity.getNoteTransaction());
                        }
                        realm.copyToRealmOrUpdate(builder.create());
                    }
                }
            });
            activity.close();
        }
    }

    public void addImage(final Bitmap bitmap) {
        if (mImages == null) {
            mImages = new RealmList<>();
        }
        if (bitmap != null) {
            activity.addImageToLayoutContainer(CameraUtils.getScaledBitmap(bitmap, 250, 250));
            Observable.create(new Observable.OnSubscribe<String>() {
                @Override
                public void call(Subscriber<? super String> subscriber) {
                    subscriber.onNext(CameraUtils.encodedImage(bitmap));
                    subscriber.onCompleted();
                }
            }).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action1<String>() {
                        @Override
                        public void call(String s) {
                            Image image = new Image();
                            image.setCreatedAt(new Date());
                            image.setImage(s);
                            mImages.add(image);
                        }
                    });
        } else {
            activity.showError(R.string.dialog_error_save_image);
        }
    }

    public void deleteImage(String id) {
        if (mImages != null) {
            for (Image image : mImages) {
                if (image.getId().equals(id)) {
                    mImages.remove(image);
                    break;
                }
            }
        }
    }

    public boolean toggleHiddenExtraFields() {
        hiddenFields = !hiddenFields;
        return hiddenFields;
    }

    @Subscribe
    public void onLocationChange(LocationService.EventLocation eventLocation) {
        this.mLocation = eventLocation.location;
    }
}
