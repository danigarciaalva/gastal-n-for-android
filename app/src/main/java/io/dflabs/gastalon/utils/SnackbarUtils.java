package io.dflabs.gastalon.utils;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import io.dflabs.gastalon.R;

/**
 * Created by Daniel García Alvarado on 9/13/15.
 * Gastalon - danielgarcia
 */
public class SnackbarUtils {

    static AppCompatActivity current;
    static View currentView;

    public static <T extends AppCompatActivity> void error(T activity, int message) {
        snackbar(activity, message, R.color.error_color);
    }

    private static <T extends AppCompatActivity> void snackbar(T activity, int message, int color) {
        if (current == null || !current.equals(activity)) {
            currentView = activity.getWindow().getDecorView().findViewById(android.R.id.content);
        }
        Snackbar snackbar = Snackbar.make(currentView, message, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(activity.getResources().getColor(color,
                activity.getTheme()));
        current = activity;
        snackbar.show();
    }
}
