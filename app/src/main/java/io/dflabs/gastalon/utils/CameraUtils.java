package io.dflabs.gastalon.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Daniel García Alvarado on 7/5/15.
 * Gastalon - danielgarcia
 */
@SuppressWarnings("unused")
public abstract class CameraUtils {

    public static final int REQUEST_TAKE_PHOTO = 0x999;
    public static final int REQUEST_IMPORT_PHOTO = 0x888;
    public static File lastPhotoPath;

    @SuppressLint("SimpleDateFormat")
    private static File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        lastPhotoPath = image;
        return image;
    }

    public static void importPhoto(AppCompatActivity context) {
        Intent i = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        context.startActivityForResult(i, REQUEST_IMPORT_PHOTO);
    }

    public static void takePhoto(AppCompatActivity context) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ignored) {

            }
            if (photoFile != null) {
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                context.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    public static Bitmap fullImageFromIntent(Context context, Intent intent) {
        if(intent != null) {
            Uri selectedImage = intent.getData();
            String[] mFilePathColumn = {MediaStore.Images.Media.DATA};
            Cursor mCursor = context.getContentResolver()
                    .query(selectedImage, mFilePathColumn, null, null, null);
            if (mCursor != null) {
                mCursor.moveToFirst();
                int mColumnIndex = mCursor.getColumnIndex(mFilePathColumn[0]);
                String mPicturePath = mCursor.getString(mColumnIndex);
                mCursor.close();
                return BitmapFactory.decodeFile(mPicturePath);
            }
        }
        return null;
    }

    public static Bitmap fullImage() {
        try {

            // Get the dimensions of the bitmap
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            bmOptions.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(lastPhotoPath.getAbsolutePath(), bmOptions);
            int photoW = bmOptions.outWidth;
            int photoH = bmOptions.outHeight;

            // Determine how much to scale down the image
            int scaleFactor = Math.min(photoW / 300, photoH / 300);

            // Decode the image file into a Bitmap sized to fill the View
            bmOptions.inJustDecodeBounds = false;
            bmOptions.inSampleSize = scaleFactor;

            return BitmapFactory.decodeFile(lastPhotoPath.getAbsolutePath(), bmOptions);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String encodedImage() {
        return encodedImage(fullImage());
    }

    public static String encodedImage(Bitmap bitmap) {
        try {
            if (bitmap != null) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return Base64.encodeToString(byteArray, Base64.DEFAULT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Bitmap encodedThumbnail() {
        return null;
    }

    public static Bitmap getScaledBitmap(Bitmap bitmap, int targetW, int targetH) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return getScaledBitmap(byteArray, targetW, targetH);
    }

    public static Bitmap getScaledBitmap(byte[] data, int targetW, int targetH) {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;

        return BitmapFactory.decodeByteArray(data, 0, data.length, bmOptions);
    }

}
