package io.dflabs.gastalon.utils;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;


import de.greenrobot.event.EventBus;

/**
 * Created by Daniel García Alvarado on 6/21/15.
 * Gastalon - danielgarcia
 */
@SuppressWarnings("unused")
public class IntentUtils {

    public static void startActivity(Context context, Class<?> activityClass, Object... events) {
        for (Object o : events) {
            EventBus.getDefault().postSticky(o);
        }
        Intent intent = new Intent(context, activityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static void startActivityForResult(AppCompatActivity context, Class<?> activityClass,
                                              int requestCode, Object... events) {
        for (Object o : events) {
            EventBus.getDefault().postSticky(o);
        }
        Intent intent = new Intent(context, activityClass);
        context.startActivityForResult(intent, requestCode);
    }

    public static void startActivityForResult(Fragment context, Class<?> activityClass,
                                              int requestCode, Object... events) {
        for (Object o : events) {
            EventBus.getDefault().postSticky(o);
        }
        Intent intent = new Intent(context.getContext(), activityClass);
        context.startActivityForResult(intent, requestCode);
    }
}
