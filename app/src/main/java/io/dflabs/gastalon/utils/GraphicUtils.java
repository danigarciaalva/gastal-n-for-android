package io.dflabs.gastalon.utils;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by Daniel García Alvarado on 9/13/15.
 * Gastalon - danielgarcia
 */
@SuppressWarnings("unused")
public class GraphicUtils {

    public static int convertPxToDp(Context context, int pixels) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(pixels / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int convertDpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
